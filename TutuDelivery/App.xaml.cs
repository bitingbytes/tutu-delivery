﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Net;
using System.IO;

namespace TutuDelivery
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static string connString = "Server=localhost;Database=tutudelivery;Uid=hitsaru;Pwd=Ffhfkkw15";
        private static MySqlConnection connection = new MySqlConnection(connString);

        public App()
        {
            connection.Open();
        }

        ~App()
        {
            if (connection.State == ConnectionState.Open)
                connection.Close();
        }

        public static MySqlConnection OpenConnection()
        {
            MySqlConnection sqlConnection = new MySqlConnection(connString);
            sqlConnection.Open();
            return sqlConnection;
        }

        public static void ExecuteQuery(string query)
        {
            MySqlCommand command = connection.CreateCommand();

            try
            {
                command.CommandText = @query;
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                throw;
            }
        }

        public static string GetResponse(Uri uri)
        {
            var request = WebRequest.Create(uri);
            request.ContentType = "application/json; charset=utf-8";
            var response = (HttpWebResponse)request.GetResponse();

            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                return sr.ReadToEnd();
            }
        }

        public static int StringToIndex(string[] text, string _string)
        {
            int Index = 0;
            foreach (string s in text)
            {
                if (s == _string)
                    return Index;
                Index++;
            }

            return -1;
        }
    }
}
