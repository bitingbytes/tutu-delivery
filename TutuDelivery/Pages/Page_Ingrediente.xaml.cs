﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TutuDelivery.Classes;

namespace TutuDelivery.Pages
{
    /// <summary>
    /// Interaction logic for Page_Ingrediente.xaml
    /// </summary>
    public partial class Page_Ingrediente : Page
    {
        public Page_Ingrediente()
        {
            InitializeComponent();
            ingredientes = new ObservableCollection<Ingrediente>();
            lstIngredientes.ItemsSource = ingredientes;
            btnClienteAdd.IsEnabled = true;
            btnClienteSave.IsEnabled = false;
            btnClienteSave.Visibility = Visibility.Hidden;
        }

        private void btnIngAdd_Click(object sender, RoutedEventArgs e)
        {
            Ingrediente ingrediente = new Ingrediente();
            ingrediente.Nome = txtNome.Text;
            ingrediente.Origem = (EOrigem)cmbTipo.SelectedIndex;
            ingrediente.Kcal = Decimal.Parse(txtKcal.Text);
            App.ExecuteQuery(ingrediente.InsertToMySql());

            MessageBox.Show("Ingrediente Cadastrado com Sucesso!");

            foreach (Control ctl in inputContainer.Children)
            {
                if (ctl.GetType() == typeof(TextBox))
                    (ctl as TextBox).Clear();
                if (ctl.GetType() == typeof(ComboBox))
                    (ctl as ComboBox).SelectedIndex = -1;
            }

            GetIngredientes();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            GetIngredientes();
        }

        private async void GetIngredientes()
        {
            MySqlConnection connection = null;
            ingredientes.Clear();
            try
            {
                connection = App.OpenConnection();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = @"SELECT * FROM tutudelivery.ingrediente;";
                var dataReader = await cmd.ExecuteReaderAsync();
                while (dataReader.Read())
                {
                    Ingrediente ingrediente = new Ingrediente();

                    ingrediente.Id = long.Parse(dataReader["id_ingrediente"].ToString());
                    ingrediente.Nome = dataReader["nome"].ToString();
                    ingrediente.Origem = dataReader["fk_origem_id"].ToString() == "1" ? EOrigem.Animal : EOrigem.Vegetal;
                    Decimal temp;
                  if( Decimal.TryParse(dataReader["kcal"].ToString(), out temp))
                    ingrediente.Kcal = temp;
                    ingredientes.Add(ingrediente);
                }
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.ToString(), "Erro");
            }
            finally
            {
                SetOrder(ingredientes);
                connection.Close();
            }

        }

        private ObservableCollection<Ingrediente> ingredientes;

        public void SetOrder(ObservableCollection<Ingrediente> data)
        {
            for (int i = 0; i < data.Count; i++)
            {
                Ingrediente ei = data[i];
                ei.Index = i;
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
          
        }

        private long editClienteId = -1;

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            btnClienteAdd.IsEnabled = false;
            btnClienteSave.IsEnabled = true;
            btnClienteSave.Visibility = Visibility.Visible;

            Button btn = sender as Button;

            int ID = int.Parse(btn.CommandParameter.ToString());

            Ingrediente ingrediente = ingredientes[ID];
            editClienteId = ingrediente.Id;

            txtNome.Text = ingrediente.Nome;
            cmbTipo.SelectedIndex = ingrediente.Origem == EOrigem.Animal ? 0 : 1;
            txtKcal.Text = ingrediente.Kcal.ToString();
        }

        private void btnDeletar_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;

            int ID = int.Parse(btn.CommandParameter.ToString());

            Ingrediente ingrediente = ingredientes[ID];
            editClienteId = ingrediente.Id;

            App.ExecuteQuery(String.Format("DELETE FROM ingrediente WHERE id_ingrediente = {0}", editClienteId));
            GetIngredientes();
        }

        private void btnCadastrar_Click(object sender, RoutedEventArgs e)
        {
            Ingrediente ingrediente = new Ingrediente();
            ingrediente.Nome = txtNome.Text;
            ingrediente.Origem = cmbTipo.SelectedIndex == 0 ? EOrigem.Animal : EOrigem.Vegetal;

            App.ExecuteQuery(ingrediente.InsertToMySql());

            foreach (Control ctl in inputContainer.Children)
            {
                if (ctl.GetType() == typeof(TextBox))
                    (ctl as TextBox).Clear();
                if (ctl.GetType() == typeof(ComboBox))
                    (ctl as ComboBox).SelectedIndex = -1;
            }

            GetIngredientes();
        }

        private void btnClienteSave_Click(object sender, RoutedEventArgs e)
        {
            if (editClienteId != -1)
            {
                using (MySqlConnection connection = App.OpenConnection())
                {
                    using (MySqlCommand cmd = connection.CreateCommand())
                    {
                        cmd.CommandText += String.Format(@"UPDATE ingrediente SET nome='{0}', kcal={1}, fk_origem_id={2} WHERE id_ingrediente={3};", txtNome.Text, txtKcal.Text.Replace(',','.'), cmbTipo.SelectedIndex + 1, editClienteId);

                        int executado = cmd.ExecuteNonQuery();
                    }
                }

                GetIngredientes();

                btnClienteAdd.IsEnabled = true;
                btnClienteSave.IsEnabled = false;
                btnClienteSave.Visibility = Visibility.Hidden;

                foreach (Control ctl in inputContainer.Children)
                {
                    if (ctl.GetType() == typeof(TextBox))
                        (ctl as TextBox).Clear();
                    if (ctl.GetType() == typeof(ComboBox))
                        (ctl as ComboBox).SelectedIndex = -1;
                }

            }
        }
    }
}
