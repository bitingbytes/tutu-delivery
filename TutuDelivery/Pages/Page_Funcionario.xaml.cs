﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TutuDelivery.Classes;

namespace TutuDelivery.Pages
{
    /// <summary>
    /// Interaction logic for Page_Funcionario.xaml
    /// </summary>
    public partial class Page_Funcionario : Page
    {
        public Page_Funcionario()
        {
            InitializeComponent();
            funcs = new List<Funcionario>();
        }

        private void btnFuncAdd_Click(object sender, RoutedEventArgs e)
        {
            Wnd_Cadastro cadastro = new Wnd_Cadastro("/Pages/Page_Cadastro_Funcionario.xaml", "Cadastro de Funcionário");
            cadastro.ShowDialog();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            GetEntregadores();
            GetCozinheiras();
            lstFunc.ItemsSource = funcs;
        }

        private async void GetEntregadores()
        {
            MySqlConnection connection = App.OpenConnection();
            MySqlCommand cmd = connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM entregador INNER JOIN funcionario ON entregador.fk_id_funcionario=funcionario.id_funcionario;";
            var dataReader = await cmd.ExecuteReaderAsync();
            while (dataReader.Read())
            {
                Funcionario funcionario = new Func_Entregador("SEM NOME");
                funcionario.Nome = dataReader["nome"].ToString();
                funcionario.Salario = Decimal.Parse(dataReader["salario"].ToString());
                funcs.Add(funcionario);
            }

            connection.CloseAsync();
        }

        private async void GetCozinheiras()
        {
            MySqlConnection connection = App.OpenConnection();
            MySqlCommand cmd = connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM cozinheira INNER JOIN funcionario ON cozinheira.fk_id_funcionario=funcionario.id_funcionario;";
            var dataReader = await cmd.ExecuteReaderAsync();
            while (dataReader.Read())
            {
                Funcionario funcionario = new Func_Cozinheira("SEM NOME");
                funcionario.Nome = dataReader["nome"].ToString();
                funcionario.Salario = Decimal.Parse(dataReader["salario"].ToString());
                funcs.Add(funcionario);
            }

            connection.CloseAsync();
        }

        private List<Funcionario> funcs;
    }
}
