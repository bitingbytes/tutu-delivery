﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TutuDelivery.Classes;

namespace TutuDelivery.Pages
{
    /// <summary>
    /// Interaction logic for Page_Cadastro_Funcionario.xaml
    /// </summary>
    public partial class Page_Cadastro_Funcionario : Page
    {
        public Page_Cadastro_Funcionario()
        {
            InitializeComponent();
        }

        private void cmbTipo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbTipo.SelectedIndex == 0)
            {
                lblSalario.Content = "Salário por hora";
            }
            else if (cmbTipo.SelectedIndex == 1)
            {
                lblSalario.Content = "Salário por entrega";
            }
        }

        private void btnCadastrar_Click(object sender, RoutedEventArgs e)
        {
            Funcionario funcionario = null;
            if (cmbTipo.SelectedIndex == 0)
            {
                funcionario = new Func_Cozinheira(txtNome.Text);
            }
            else if (cmbTipo.SelectedIndex == 1)
            {
                funcionario = new Func_Entregador(txtNome.Text);
            }

            if (txtSalario.Text.Length > 0)
                funcionario.Salario = Decimal.Parse(txtSalario.Text);

            App.ExecuteQuery(funcionario.InsertToMySql());

            MessageBox.Show("Funcionário Cadastrado com Sucesso!");

            foreach (Control ctl in inputContainer.Children)
            {
                if (ctl.GetType() == typeof(TextBox))
                    (ctl as TextBox).Clear();
                if (ctl.GetType() == typeof(ComboBox))
                    (ctl as ComboBox).SelectedIndex = -1;
            }
        }
    }
}
