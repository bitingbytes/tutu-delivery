﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TutuDelivery.Classes;

namespace TutuDelivery.Pages
{
    /// <summary>
    /// Interaction logic for Page_Cliente.xaml
    /// </summary>
    public partial class Page_Cliente : Page
    {
        public Page_Cliente()
        {
            InitializeComponent();
            clientes = new ObservableCollection<Cliente>();
            cmbEstados.ItemsSource = SiglaEstados;
            lstClientes.ItemsSource = clientes;
            btnClienteAdd.IsEnabled = true;
            btnClienteSave.IsEnabled = false;
            btnClienteSave.Visibility = Visibility.Hidden;
        }

        public string[] SiglaEstados = new string[] { "AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", "RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO" };

        private void btnCadastrar_Click(object sender, RoutedEventArgs e)
        {
            /*string Logradouro = txtLogradouro.Text;
            uint Numero = uint.Parse(txtNumero.Text);
            string Complemento = txtComplemento.Text;
            string Bairro = txtBairro.Text;
            string Cidade = txtCidade.Text;
            string Estado = cmbEstados.SelectedValue.ToString();
            CEndereco endereco = new CEndereco(Logradouro, Numero, Complemento, Bairro, Cidade, Estado);
            */
            CTelefone telefone = new CTelefone(txtTelefone.Text);

            Cliente cliente = new Cliente(txtNome.Text, txtEmail.Text);

            App.ExecuteQuery(cliente.InsertToMySql());

            App.ExecuteQuery(@String.Format("INSERT INTO TELEFONE (`ddd`, `numero`, `fk_id_cliente`) VALUES ({0}, {1}, (SELECT MAX(id_cliente) from cliente));", telefone.DDD.ToString(), telefone.Numero.ToString()));

            foreach (Control ctl in inputContainer.Children)
            {
                if (ctl.GetType() == typeof(TextBox))
                    (ctl as TextBox).Clear();
                if (ctl.GetType() == typeof(ComboBox))
                    (ctl as ComboBox).SelectedIndex = -1;
            }

            GetClientes();
        }

        public void SetCliente(Cliente cliente)
        {
            txtNome.Text = cliente.Nome;
        }

        private void txtCEP_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(txtCEP.Text))
            {
                string enderecoCEP = App.GetResponse(new Uri(String.Format("https://viacep.com.br/ws/{0}/json/", txtCEP.Text)));
                dynamic json = JsonConvert.DeserializeObject(enderecoCEP);
                string cep = json.cep;
                string logradouro = json.logradouro;
                string complemento = json.complemento;
                string bairro = json.bairro;
                string cidade = json.localidade;
                string estado = json.uf;

                txtCEP.Text = cep;
                txtLogradouro.Text = logradouro;
                txtComplemento.Text = complemento;
                txtBairro.Text = bairro;
                txtCidade.Text = cidade;
                cmbEstados.SelectedIndex = App.StringToIndex(SiglaEstados, estado);
            }
        }

        private void pgCliente_Loaded(object sender, RoutedEventArgs e)
        {
            GetClientes();
        }

        private async void GetClientes()
        {
            clientes.Clear();
            MySqlConnection connection = App.OpenConnection();
            MySqlCommand cmd = connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM cliente";
            var dataReader = await cmd.ExecuteReaderAsync();
            while (dataReader.Read())
            {
                Cliente cliente = new Cliente();
                string id = dataReader["id_cliente"].ToString();
                cliente.Id = long.Parse(id);

                MySqlConnection connectionT = App.OpenConnection();
                MySqlCommand cmdT = connectionT.CreateCommand();
                cmdT.CommandText = @String.Format("SELECT * FROM telefone WHERE fk_id_cliente = {0}", id);
                var dataReaderT = await cmdT.ExecuteReaderAsync();
                CTelefone telefone = null;
                if (dataReaderT.Read())
                    telefone = new CTelefone(dataReaderT["ddd"].ToString() + dataReaderT["numero"].ToString());

                cliente.Telefone = telefone;
                cliente.Nome = dataReader["nome"].ToString();
                cliente.Email = dataReader["email"].ToString();
                clientes.Add(cliente);
            }
            SetOrder(clientes);
            connection.CloseAsync();
        }

        private ObservableCollection<Cliente> clientes;

        public void SetOrder(ObservableCollection<Cliente> data)
        {
            for (int i = 0; i < data.Count; i++)
            {
                Cliente ei = data[i];
                ei.Index = i;
            }
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            btnClienteAdd.IsEnabled = false;
            btnClienteSave.IsEnabled = true;
            btnClienteSave.Visibility = Visibility.Visible;

            Button btn = sender as Button;

            int ID = int.Parse(btn.CommandParameter.ToString());

            Cliente cliente = clientes[ID];
            editClienteId = cliente.Id;

            txtNome.Text = cliente.Nome;
            txtEmail.Text = cliente.Email;
            txtTelefone.Text = cliente.Telefone.ToString();
        }

        private void btnClienteSave_Click(object sender, RoutedEventArgs e)
        {
            if (editClienteId != -1)
            {
                using (MySqlConnection connection = App.OpenConnection())
                {
                    using (MySqlCommand cmd = connection.CreateCommand())
                    {
                        CTelefone tel = new CTelefone(txtTelefone.Text);
                        cmd.CommandText = String.Format(@"UPDATE telefone SET ddd={0}, numero={1} WHERE numero={1} AND fk_id_cliente={2};", tel.DDD, tel.Numero, editClienteId);
                        cmd.CommandText += String.Format(@"UPDATE cliente SET nome='{0}', email='{1}' WHERE id_cliente={2};", txtNome.Text, txtEmail.Text, editClienteId);

                        int executado = cmd.ExecuteNonQuery();
                    }
                }

                GetClientes();

                btnClienteAdd.IsEnabled = true;
                btnClienteSave.IsEnabled = false;
                btnClienteSave.Visibility = Visibility.Hidden;

                foreach (Control ctl in inputContainer.Children)
                {
                    if (ctl.GetType() == typeof(TextBox))
                        (ctl as TextBox).Clear();
                    if (ctl.GetType() == typeof(ComboBox))
                        (ctl as ComboBox).SelectedIndex = -1;
                }

            }
        }

        private long editClienteId = -1;

        private void btnDeletar_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;

            int ID = int.Parse(btn.CommandParameter.ToString());

            Cliente cliente = clientes[ID];
            editClienteId = cliente.Id;

            App.ExecuteQuery(String.Format("DELETE FROM cliente WHERE id_cliente = {0}", editClienteId));
            GetClientes();
        }
    }
}
