﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TutuDelivery.Pages
{
    /// <summary>
    /// Interaction logic for Wnd_Cadastro.xaml
    /// </summary>
    public partial class Wnd_Cadastro : Window
    {
        public Wnd_Cadastro(string uri, string titulo)
        {
            InitializeComponent();
            CurrentUri = uri;
            Titulo.Content = titulo;
            Title = titulo;
        }

        public object Componente { set; get; }

        private string CurrentUri;

        private void Window_Activated(object sender, EventArgs e)
        {
            frmCadastro.Source = new Uri(CurrentUri, UriKind.Relative);
        }
    }
}
