﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TutuDelivery.Classes
{
    public class Func_Cozinheira : Funcionario
    {
        public Func_Cozinheira(string nome) : base(nome)
        {
            Cargo = "Cozinheira";
            HorasDiarias = new TimeSpan();
        }

        public TimeSpan HorasDiarias { set; get; }

        public override string InsertToMySql()
        {
            return String.Format("INSERT INTO `funcionario` (`nome`, `salario`) VALUES ('{0}', {1}); INSERT INTO `cozinheira` (`fk_id_funcionario`, `horas_diarias`) VALUES ((SELECT MAX(id_funcionario) from funcionario)  , '{2}');", Nome, Salario.ToString().Replace(',', '.'), HorasDiarias.Hours.ToString());
        }
    }
}
