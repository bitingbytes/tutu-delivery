﻿using System;

namespace TutuDelivery.Classes
{
    public class CEndereco
    {
        public CEndereco(string logradouro)
        {
            Logradouro = logradouro ?? throw new ArgumentNullException(nameof(logradouro));
        }

        public CEndereco(string logradouro, uint numero, string complemento, string bairro, string cidade, string estado)
            : this(logradouro)
        {
            Numero = numero;
            Complemento = complemento;
            Bairro = bairro;
            Cidade = cidade;
            Estado = estado;
        }

        public string Logradouro { set; get; }
        public uint Numero { set; get; }
        public string Complemento { set; get; }
        public string Bairro { set; get; }
        public string Cidade { set; get; }
        public string Estado { set; get; }
    }
}