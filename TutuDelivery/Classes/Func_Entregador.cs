﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TutuDelivery.Classes
{
    public class Func_Entregador : Funcionario
    {
        public Func_Entregador(string nome) 
            : base(nome)
        {
            Cargo = "Entregador";
            HorasTrabalhadas = new TimeSpan();
        }

        public TimeSpan HorasTrabalhadas { set; get; }

        public override string InsertToMySql()
        {
            return String.Format("INSERT INTO `funcionario` (`nome`, `salario`) VALUES ('{0}', {1}); INSERT INTO `entregador` (`fk_id_funcionario`, `horas_trabalhadas`) VALUES ((SELECT MAX(id_funcionario) from funcionario)  , '{2}');", Nome, Salario.ToString().Replace(',','.'), HorasTrabalhadas.Hours.ToString());
        }
    }
}
