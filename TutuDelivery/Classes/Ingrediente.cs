﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TutuDelivery.Classes
{
    [Flags]
    public enum EOrigem : uint
    {
        [Display(Name = "Animal")]
        Animal = 0,
        [Display(Name = "Vegetal")]
        Vegetal = 1
    }

    public class Ingrediente : IIndex
    {
        public long Id { set; get; }
        public string Nome { set; get; }
        public EOrigem Origem { set; get; }
        public Decimal Kcal { set; get; } = 0;

        public string InsertToMySql()
        {
            return String.Format(@"INSERT INTO ingrediente(`nome`, `fk_origem_id`, `kcal`) VALUES ('{0}', {1}, {2});", Nome, Origem == EOrigem.Animal ? 1 : 2, Kcal.ToString().Replace(',', '.'));
        }
    }
}
