﻿namespace TutuDelivery.Classes
{
    public class CTelefone
    {
        public CTelefone(string _numero)
        {
            string sddd = _numero.Substring(0, 2);
            string snumero = _numero.Substring(2);
            DDD = byte.Parse(sddd);
            Numero = uint.Parse(snumero);
        }

        public CTelefone(byte _DDD, uint numero)
        {
            DDD = _DDD;
            Numero = numero;
        }

        public byte DDD { set; get; }
        public uint Numero { set; get; }

        public override bool Equals(object obj)
        {
            CTelefone telefoneObj = obj as CTelefone;
            return DDD.Equals(telefoneObj.DDD) && Numero.Equals(telefoneObj.Numero);
        }

        public override int GetHashCode()
        {
            return DDD.GetHashCode() + Numero.GetHashCode();
        }

        public override string ToString()
        {
            return DDD.ToString() + Numero.ToString();
        }
    }
}