﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TutuDelivery.Classes
{
    public class Cliente : IIndex
    {
        public Cliente()
        {
            Nome = null;
            Email = null;
            Telefone = null;
            Endereco = null;
        }

        public Cliente(string nome)
        {
            Nome = nome ?? throw new ArgumentNullException(nameof(nome));
            Email = null;
        }

        public Cliente(string nome, string email)
            : this(nome)
        {
            Email = email;
        }

        public long Id { set; get; }
        public string Nome { set; get; }
        public string Email { set; get; }
        public CTelefone Telefone { set; get; }
        public CEndereco Endereco { set; get; }

        public override bool Equals(object obj)
        {
            Cliente clienteObj = obj as Cliente;
            if (clienteObj == null)
                return false;
            else
                return Telefone.Equals(clienteObj.Telefone);
        }

        public override int GetHashCode()
        {
            return Telefone.GetHashCode();
        }

        public override string ToString()
        {
            return Nome + " | " + Telefone;
        }

        public string InsertToMySql()
        {
            return String.Format("INSERT INTO `cliente` (`nome`, `email`, `fk_id_logradouro`) VALUES ('{0}', '{1}', {2});", Nome, Email, 1);
        }
    }
}
