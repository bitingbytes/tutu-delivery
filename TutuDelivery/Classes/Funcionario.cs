﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TutuDelivery.Classes
{
    public abstract class Funcionario
    {
        protected Funcionario(string nome)
        {
            Nome = nome ?? throw new ArgumentNullException(nameof(nome));
        }

        public Funcionario(string nome, decimal salario)
            : this(nome)
        {
            Salario = salario;
        }

        public string Nome { set; get; }

        public string Cargo { set; get; }

        public Decimal Salario { set; get; }

        public abstract string InsertToMySql();
    }
}
