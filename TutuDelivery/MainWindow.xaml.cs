﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TutuDelivery
{

    public enum EActivedPage : byte
    {
        AP_PEDIDOS = 0,
        AP_MARMITA = 1,
        AP_INGREDIENTE = 2,
        AP_CLIENTE = 3,
        AP_FUNCIONATIO = 4
    }

    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            GotoPage();
        }

        /// <summary>
        /// Pagina ativa no momento.
        /// </summary>
        public EActivedPage PaginaAtiva { set; get; } = EActivedPage.AP_PEDIDOS;

        private void btnPedido_Click(object sender, RoutedEventArgs e)
        {
            PaginaAtiva = EActivedPage.AP_PEDIDOS;
            GotoPage();
        }

        private void btnMarmita_Click(object sender, RoutedEventArgs e)
        {
            PaginaAtiva = EActivedPage.AP_MARMITA;
            GotoPage();
        }

        private void btnIngrediente_Click(object sender, RoutedEventArgs e)
        {
            PaginaAtiva = EActivedPage.AP_INGREDIENTE;
            GotoPage();
        }

        private void btnCliente_Click(object sender, RoutedEventArgs e)
        {
            PaginaAtiva = EActivedPage.AP_CLIENTE;
            GotoPage();
        }

        private void btnFuncionario_Click(object sender, RoutedEventArgs e)
        {
            PaginaAtiva = EActivedPage.AP_FUNCIONATIO;
            GotoPage();
        }

        private void GotoPage()
        {
            fmMainFrame.Source = new Uri(PagePaths[(int)PaginaAtiva], UriKind.Relative);
        }

        private void SelectFromMenu()
        {

        }

        private string[] PagePaths = new string[5]
         {
            "/Pages/Page_Pedidos.xaml",
            "/Pages/Page_Marmita.xaml",
            "/Pages/Page_Ingrediente.xaml",
            "/Pages/Page_Cliente.xaml",
            "/Pages/Page_Funcionario.xaml"
         };
    }
}
